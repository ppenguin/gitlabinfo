#!/usr/bin/env bash

set -e

SNAME=$(basename ${0})
PROJ="${1}"
BRANCH="${2}"
CMSG="${COMMITMSG:-auto commit}"

TMPPFX=${TMPPFX:-./}
[[ -d "${TMPPFX}" ]] || mkdir -p "${TMPPFX}"
echo "Using TMPPFX==${TMPPFX}"
TMP="$(mktemp -p "${TMPPFX}" -d)"

usage(){
    echo -e "Usage:\n"
    echo -e "\t${SNAME} git_project_path branch_name\n"
    echo -e "\n\tSetting env vars GIT_BASE_URL=<url> SOURCE=<source_dir_or_glob> TDIR=<relative_target_dir> is required\n"
    echo -e "\n\tand COMMITMSG=<commit_message> optional (default is \"auto commit\")\n"
    echo -e "\n\t${SNAME} will clone the given project, recursively copy the contents of SDIR to TDIR, commit and push\n"
    echo -e "\n\tOptionally set TMPPFX to an alternative root for the temp dir"
}

exiterr() {
    usage
    exit 1
}

cleanup() {
    echo -n "Cleaning up... "
    [[ -n "${TMP}" ]] && rm -rf ${TMP} || :
    echo "done, exit (${EC})"
}
trap cleanup EXIT

if [ -z "${GIT_BASE_URL}" ] || [ -z "${SOURCE}" ] || [ -z "${TDIR}" ] || [ $# -lt 2 ]; then
    exiterr
fi

git clone ${GIT_BASE_URL}/${PROJ}.git ${TMP}
cd ${TMP}
git checkout -b ${BRANCH}
[[ -d "${TDIR}" ]] || mkdir -p "${TDIR}"
eval cp -av "${SOURCE}" "${TDIR}"/
git add ${TDIR}/\*
git commit -am "${CMSG}"
git push -u origin ${BRANCH}
