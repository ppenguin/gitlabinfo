# gitlabinfo

A quick&dirty python script leveraging the python-gitlabmodule and the gitlab api.
For now only supports some basic listings (useful for e.g. self-hosted larger group-project trees), to be expanded as needed with more functionality.