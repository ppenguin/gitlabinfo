#!/usr/bin/env python

import os, subprocess, time
import gitlab
from argparse import ArgumentParser
import re
from gitlabinfo import filteredProjects

# Somehow we cannot import the subcommand stuff from gitlabinfo, so redefine here...

# some global settings
cli = ArgumentParser(description="gitlab info util")
subparsers = cli.add_subparsers(dest="subcommand")

def argument(*name_or_flags, **kwargs):
    """Convenience function to properly format arguments to pass to the
    subcommand decorator.
    """
    return (list(name_or_flags), kwargs)


def subcommand(args=[], parent=subparsers):
    """Decorator to define a new subcommand in a sanity-preserving way.
    The function will be stored in the ``func`` variable when the parser
    parses arguments so that it can be called directly like so::
        args = cli.parse_args()
        args.func(args)
    Usage example::
        @subcommand([argument("-d", help="Enable debug mode", action="store_true")])
        def subcommand(args):
            print(args)
    Then on the command line::
        $ python cli.py subcommand -d
    """
    def decorator(func):
        parser = parent.add_parser(func.__name__, description=func.__doc__)
        for arg in args:
            parser.add_argument(*arg[0], **arg[1])
        parser.set_defaults(func=func)
    return decorator

@subcommand([
        argument("-f", "--filter", default="", type=str, help="Filter regex for projects to create mr for"),
        argument("-s", "--source", default="mass-auto-branch", type=str, help="Source branch of the new MR"),
        argument("-t", "--target", default="main", type=str, help="Target branch of the new MR"),
        argument("--title", default="automatic mass MR", type=str, help="MR title"),
        argument("-l", "--labels", default="", type=str, help="Comma-separated list of labels"),
        argument("-x", "--exec", default="", type=str, help="Execute shell command or script after creating MR"),
        argument("-m", "--merge", default=True, type=bool, help="Do a merge after executing the --exec command"),
        argument("--mrdelay", default=15, type=int, help="Delay in seconds to wait between MRs (to avoid rate limits)"),
        argument("--mdelay", default=5, type=int, help="Delay in seconds to wait before merge (to avoid rate limits)")
    ])
def mrmulticreate(args):
    gl = gitlab.Gitlab(args.glURL, args.glToken)
    rx = re.compile(args.filter)
    fp = filteredProjects(gl, rx)

    labels = args.labels.split(',')

    print("\nCreating merge request for selected projects:\n")
    for p in fp:
        print(f"{os.path.join(p.namespace['full_path'],p.path)}:")
        try:
            print(f"Waiting {args.mrdelay} seconds before MR to avoid rate limits...")
            time.sleep(args.mrdelay)  # we do this at the start to make sure any ignored errors in previous iterations don't bypass the delay
            mr = p.mergerequests.create({
                'source_branch': args.source,
                'target_branch': args.target,
                'title': args.title,
                'labels': labels,
                'merge_when_pipeline_succeeds': True,
                'should_remove_source_branch': True
            })
        except Exception as e:
            print(f"Error creating MR: {e}, moving on...")
            continue

        if mr.id >0:
            print(f"Created MR with id {mr.id}:")
        else:
            print(f"No MR obtained, something went wrong...")
            continue

        if not args.merge:
            continue

        if args.exec == "":
            print("No exec command, nothing to do for merge.")
            continue

        # usually this will just be a script which pulls most info from env vars and takes 2 pos args:
        # (1) full repo path; (2) the source branch of the merge request
        procargs = args.exec.split(" ")
        procargs.append(os.path.join(p.namespace['full_path'],p.path))
        procargs.append(args.source)
        print(f"Running \"{' '.join(procargs)}\" on MR creation")
        ret = subprocess.run(procargs)
        # print(f"External exec returned: {ret}")
        if ret.returncode != 0:
            print(f"An error occurred while executing \"{' '.join(procargs)}\"; skipping merge and deleting MR")
            try:
                mr.delete()
            except Exception as de:
                print(f"MR delete failed ({de}), ignoring and continuing with next")
                pass
            continue
        print(f"Pushed changes, delaying {args.mdelay} seconds before merge...")
        time.sleep(args.mdelay)
        print(f"Executing merge...")
        try:
            mr.merge()
            print(f"MR merged, deleting source branch...")
            try:
                b = p.branches.get(args.source)
                b.delete()
            except Exception as be:
                print(f"Error deleting merged source branch {args.source} ({be})")
                pass
        except Exception as me:
            print(f"Merge failed ({me})! Trying to delete MR..")
            try:
                mr.delete()
                print(f"MR deleted")
            except Exception as de:
                print(f"Delete failed ({de}), ignoring and continuing with next")
                pass


if __name__ == '__main__':

    cli.add_argument('--url', dest='glURL', required=True, type=str, help="base url for your gitlab instance")
    cli.add_argument('--token', dest='glToken', required=True, type=str, help="your gitlab private access token")
    args = cli.parse_args()

    if args.subcommand is None:
        cli.print_help()
    else:
        args.func(args)
