#!/usr/bin/env python

# Quick&Dirty actions on gitlab repo
# based on https://stackoverflow.com/a/57051024/12771809

# cool trick to make very compact cli:
# https://gist.github.com/mivade/384c2c41c3a29c637cb6c603d4197f9f

import os
import gitlab
from argparse import ArgumentParser
import re

# some global settings
cli = ArgumentParser(description="gitlab info util")
subparsers = cli.add_subparsers(dest="subcommand")

def argument(*name_or_flags, **kwargs):
    """Convenience function to properly format arguments to pass to the
    subcommand decorator.
    """
    return (list(name_or_flags), kwargs)


def subcommand(args=[], parent=subparsers):
    """Decorator to define a new subcommand in a sanity-preserving way.
    The function will be stored in the ``func`` variable when the parser
    parses arguments so that it can be called directly like so::
        args = cli.parse_args()
        args.func(args)
    Usage example::
        @subcommand([argument("-d", help="Enable debug mode", action="store_true")])
        def subcommand(args):
            print(args)
    Then on the command line::
        $ python cli.py subcommand -d
    """
    def decorator(func):
        parser = parent.add_parser(func.__name__, description=func.__doc__)
        for arg in args:
            parser.add_argument(*arg[0], **arg[1])
        parser.set_defaults(func=func)
    return decorator


@subcommand([argument("-f", "--filter", default=".*", help="Filter regex for projects listing")])
def test(args):
    print(f"args.filter == {args.filter}")


@subcommand([argument("-f", "--filter", default=".*", help="Filter regex for projects listing")])
def lsprojects(args):
    gl = gitlab.Gitlab(args.glURL, args.glToken)
    rx = re.compile(args.filter)
    print("\nProjects:\n")
    for p in filteredProjects(gl, rx):
        print(os.path.join(p.namespace['full_path'],p.path))


@subcommand([argument("-f", "--filter", default=".*", help="Filter regex for groups listing")])
def lsgroups(args):
    gl = gitlab.Gitlab(args.glURL, args.glToken)
    rx = re.compile(args.filter)
    print("\nGroups:\n")
    for g in gl.groups.list(all=True):
        if rx.match(g.full_path) is not None:
            print(g.full_path)


@subcommand([argument("-f", "--filter", default=".*", help="Filter regex for projects to list hooks for")])
def lsprojecthooks(args):
    gl = gitlab.Gitlab(args.glURL, args.glToken)
    rx = re.compile(args.filter)
    print("\nHooks:\n")
    for p in filteredProjects(gl, rx):
        print(f"Hooks for project {os.path.join(p.namespace['full_path'],p.path)}:")
        for h in p.hooks.list():
            print(h)


@subcommand([argument("-f", "--filter", default=".*", help="Filter regex for projects to list labels for")])
def lsprojectlabels(args):
    gl = gitlab.Gitlab(args.glURL, args.glToken)
    rx = re.compile(args.filter)
    print("\nLabels:\n")
    for p in filteredProjects(gl, rx):
        print(f"Labels for project {os.path.join(p.namespace['full_path'],p.path)}:")
        for l in projectLabels(p):
            print(l.name)


@subcommand([argument("-f", "--filter", default=".*", help="Filter regex for projects to list branches for")])
def lsprojectbranches(args):
    gl = gitlab.Gitlab(args.glURL, args.glToken)
    rx = re.compile(args.filter)
    print("\nBranches:\n")
    for p in filteredProjects(gl, rx):
        print(f"Branches of project {os.path.join(p.namespace['full_path'],p.path)}:")
        for b in projectBranches(p):
            print(f"{b.name} (branch of {os.path.join(p.namespace['full_path'],p.path)})")


@subcommand([
                argument("-f", "--filter", default=".*", help="Filter regex for projects to delete branches for"),
                argument("-n", "--name", default="", help="Name of branch to be deleted")
            ])
def delprojectbranches(args):
    gl = gitlab.Gitlab(args.glURL, args.glToken)
    rx = re.compile(args.filter)
    print("\nBranches:\n")
    for p in filteredProjects(gl, rx):
        # print(f"Labels for project {os.path.join(p.namespace['full_path'],p.path)}:")
        for b in projectBranches(p):
            if b.name == args.name:
                print(f"Deleting branch \"{b.name}\" of project \"{os.path.join(p.namespace['full_path'],p.path)}\"")
                b.delete()


@subcommand([
                argument("-f", "--filter", default=".*", help="Filter regex for projects to delete labels for"),
                argument("-n", "--name", default="", help="Name of label to be deleted")
            ])
def delprojectlabels(args):
    gl = gitlab.Gitlab(args.glURL, args.glToken)
    rx = re.compile(args.filter)
    print("\nLabels:\n")
    for p in filteredProjects(gl, rx):
        # print(f"Labels for project {os.path.join(p.namespace['full_path'],p.path)}:")
        for l in projectLabels(p):
            if l.name == args.name:
                print(f"Deleting label \"{l.name}\" of project \"{os.path.join(p.namespace['full_path'],p.path)}\"")
                l.delete()


@subcommand([
                argument("-f", "--filter", default=".*", type=str, help="Filter regex for projects to modify MR for"),
                argument("-t", "--title", default="", type=str, help="Title of the MR to modify"),
                argument("-d", "--delete", default=False, type=bool, help="Delete matching MR"),
                argument("-s", "--state", default="opened", type=str, help="State of MRs to match")
                # TODO: more modification functionality
            ])
def mrmodbytitle(args):
    gl = gitlab.Gitlab(args.glURL, args.glToken)
    rx = re.compile(args.filter)
    mrx = re.compile(args.title)
    print("\nModify MRs:\n")
    for p in filteredProjects(gl, rx):
        for mr in p.mergerequests.list(state=args.state):
            # print(f"\tEvaluating MR \"{mr.title}\" of project \"{os.path.join(p.namespace['full_path'],p.path)}\"...")
            if mrx.match(mr.title) is not None:
                print(f"MR with title \"{mr.title}\" in \"{os.path.join(p.namespace['full_path'],p.path)}\" matches")
                if args.delete:
                    mr.delete()
                    print(f"\tDeleted MR \"{mr.title}\" in \"{os.path.join(p.namespace['full_path'],p.path)}\"")
                # l.delete()


def projectLabels(project):
    labels = []
    for l in project.labels.list():
        if l.is_project_label:
            labels.append(l)
    return labels


def projectBranches(project):
    branches = []
    for b in project.branches.list():
        branches.append(b)
    return branches


def filteredProjects(glinstance, regex):
    fp = []
    projects = glinstance.projects.list(all=True)
    length=len(projects)
    i=0
    while i < length:
        project = glinstance.projects.get(projects[i].id)
        fullpath = os.path.join(project.namespace['full_path'],project.path)
        if regex.match(fullpath) is not None:
            fp.append(project)
        i=i+1
    return fp


def projectByPath(glinstance, fullpath):
    projects = glinstance.projects.list(all=True)
    length=len(projects)
    i=0
    while i < length:
        project = glinstance.projects.get(projects[i].id)
        fp = os.path.join(project.namespace['full_path'],project.path)
        if fp == fullpath:
            return project
        i=i+1
    return None


@subcommand([argument("-p", "--project", help="Full project path"),
             argument("-u", "--url", help="Full url for the web hook"),
             argument("--push", default=True, help="Push events"),
             argument("--tagpush", default=True, help="Tag push events"),
             argument("--merge", default=True, help="Merge request events"),
             argument("--repo", default=True, help="Repo update events"),
             argument("--issue", default=True, help="Issue events"),
             argument("--note", default=True, help="Note events"),
             argument("--pipeline", default=True, help="Pipeline events"),
             argument("--job", default=False, help="Job events"),
             argument("--deploy", default=True, help="Deployment events"),
             argument("--release", default=True, help="Release events"),
             argument("--wiki", default=True, help="Wiki events"),
             argument("--confissue", default=False, help="Confidential issue events"),
             argument("--confnote", default=False, help="Confidential note events"),
             argument("--ssl", default=True, help="Enable ssl verification"),
            ])
def createprojecthook(args):
    gl = gitlab.Gitlab(args.glURL, args.glToken)
    p = projectByPath(gl, args.project)
    if p is None:
        print(f"Project {args.project} not found")
    else:
        h = p.hooks.create({
            'url': args.url,
            'push_events': args.push,
            'tag_push_events': args.tagpush,
            'merge_requests_events': args.merge,
            'repository_update_events': args.repo,
            'enable_ssl_verification': args.ssl,
            'issues_events': args.issue,
            'confidential_issues_events': args.confissue,
            'note_events': args.note,
            'confidential_note_events': args.confnote,
            'pipeline_events': args.pipeline,
            'wiki_page_events': args.wiki,
            'deployment_events': args.deploy,
            'job_events': args.job,
            'releases_events': args.release,
            # 'push_events_branch_filter': ''
        })
    if h is None:
        print("Failed creating hook.")
    else:
        print("Hook created.")


if __name__ == '__main__':

    cli.add_argument('--url', dest='glURL', required=True, type=str, help="base url for your gitlab instance")
    cli.add_argument('--token', dest='glToken', required=True, type=str, help="your gitlab private access token")
    args = cli.parse_args()

    if args.subcommand is None:
        cli.print_help()
    else:
        args.func(args)
